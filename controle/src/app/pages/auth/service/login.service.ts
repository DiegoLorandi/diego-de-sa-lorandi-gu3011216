import {AngularFireAuth, User } from '@angular/fire/auth'
import { Injectable } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn: Observable<User>

  constructor(
    private nav:NavController,
    private auth: AngularAuthFire,
    private toast:ToastController,
  ) {
    this.isLoggedIn = this.auth.authState;
   }


  login(user){
    this.auth.sighInWithEmailAndPassword(user.email,user.password).
    then(()=>this.nav.navigateForward('home')).
    catch(()=> this.showError())
  }

  private async showError() {
    const ctrl = await this.toast.create({
      message:"Dados Incorretos",
      duration:3000
    })
    ctrl.present();
  }

  recoverPass(data){
    this.auth.sendPasswordResetEmail(data.email).
    then(()=> this.nav.navigateBack('login')).
    catch(err => {
      console.log(err);
    } );
  }

  logout(){
    this.auth.sighOut().
    then(()=>this.nav.navigateBack('auth'));
  }

  createUser(user){
    this.auth.createUserWithEmailAndPassword(user.email,user.password);
  }
}
