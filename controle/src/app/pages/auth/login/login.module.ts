import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginPageRoutingModule } from './login-routing.module';

import { LoginPage } from './login.page';
import { TopoLoginComponent } from '../component/component/topo-login/topo-login.component';
import { ComponentModule } from 'src/app/pages/auth/component/component/component.module';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    ComponentModule,
    FormsModule,
    IonicModule,
    LoginPageRoutingModule
  ],
  declarations: [LoginPage]
})
export class LoginPageModule {}
