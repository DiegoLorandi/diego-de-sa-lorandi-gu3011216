// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyBSBBaKij4ZzZF7KnYdv2EAt3VwBSQThzw',
    authDomain: 'controle-diego.firebaseapp.com',
    projectId: 'controle-diego',
    storageBucket: 'controle-diego.appspot.com',
    messagingSenderId: '720573734055',
    appId: '1:720573734055:web:cb803238beb4db5ff6cf82',
    measurementId: 'G-R9Q1B4L66W'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
